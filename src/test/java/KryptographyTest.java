import static org.junit.Assert.assertEquals;

import com.klabs.kryptography.Kryptography;
import com.klabs.kryptography.exception.DecryptException;
import com.klabs.kryptography.exception.EncryptException;
import com.klabs.kryptography.model.AlgorithmCipherEnum;
import com.klabs.kryptography.model.SecretKeyAlgorithmEnum;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class KryptographyTest {

    private Kryptography kryptography;

    @Before
    public void setup(){
        kryptography = new Kryptography();
    }

    @Test
    public void givenString_whenEncryptWithDESede_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DESede_CBC_NoPadding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DESede;
        String valueToCipher = "42424242";
        String key = "424242424242424242424242";
        String initialValue = "42424242";

        String cipherValue = kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue);
        assertEquals("oTTr4KAk52E=", cipherValue);
    }

    @Test
    public void givenString_whenEncryptWithDES_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "answer to everything";
        String key = "42424242";
        String initialValue = "42424242";

        String cipherValue = kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue);
        assertEquals("4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU", cipherValue);
    }


    @Test
    public void givenStringEncrypt_whenDecryptWithDES_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU";
        String key = "42424242";
        String initialValue = "42424242";

        String decipherValue = kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue);
        assertEquals("answer to everything", decipherValue);
    }

    @Test
    public void notGivenKey_whenDecrypt_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU";
        String key = "";
        String initialValue = "42424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Empty key", message);
    }

    @Test
    public void notGivenKey_whenEncrypt_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU";
        String key = "";
        String initialValue = "42424242";

        String message = assertThrows(EncryptException.class,
                () -> kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue)).getMessage();

        assertEquals("Empty key", message);
    }

    @Test
    public void notGivenInitialValue_whenDecrypt_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU";
        String key = "42424242";
        String initialValue = "";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Wrong IV length: must be 8 bytes long", message);
    }

    @Test
    public void givenInvalidEncriptedValue_whenDecryptAES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "SC0ikjOd42eitaGzzOlClA==";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Given final block not properly padded. Such issues can arise if a bad key is used during decryption.", message);
    }

    @Test
    public void givenInvalidEncriptedValue_whenDecryptWithDES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "4rdeuTUMq1jGZuqSrD9Ddh843hBcbGwU";
        String key = "42424242";
        String initialValue = "42424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Given final block not properly padded. Such issues can arise if a bad key is used during decryption.", message);

    }

    @Test
    public void givenWrongValueToCipher_whenEncryptAES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToCipher = "";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(EncryptException.class,
                () -> kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue)).getMessage();

        assertEquals("The value provided to be decrypt/encrypt was not filled in properly.", message);
    }

    @Test
    public void givenWrongValueToDecipher_whenEncryptAES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("The value provided to be decrypt/encrypt was not filled in properly.", message);
    }

    @Test
    public void givenWrongAlgorithmEncryptionToCipher_whenEncrypt_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "this is a raining day";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(EncryptException.class,
                () -> kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: AES or Rijndael required", message);
    }

    @Test
    public void givenWrongAlgorithmSecretKeyToCipher_whenEncryptAES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "this is a raining day";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(EncryptException.class,
                () -> kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: AES or Rijndael required", message);
    }

    @Test
    public void givenWrongAlgorithmSecretKeyToCipher_whenEncryptDESede_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DESede_CBC_NoPadding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToCipher = "1234";
        String key = "424242424242424242424242";
        String initialValue = "42424242";

        String message = assertThrows(EncryptException.class,
                () -> kryptography.encrypt(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: DESede or TripleDES required", message);
    }

    @Test
    public void givenWrongAlgorithmEncryptionToDecipher_whenDecrypt_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DESede_CBC_NoPadding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "this is a raining day";
        String key = "424242424242424242424242";
        String initialValue = "42424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: DESede or TripleDES required", message);
    }

    @Test
    public void givenWrongAlgorithmSecretKeyToDecipher_whenDecryptAES_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DESede;
        String valueToDecipher = "this is a raining day";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: AES or Rijndael required", message);
    }

    @Test
    public void givenWrongAlgorithmSecretKeyToDecipher_whenDecryptDESede_thenException(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DESede_CBC_NoPadding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "1234";
        String key = "424242424242424242424242";
        String initialValue = "42424242";

        String message = assertThrows(DecryptException.class,
                () -> kryptography.decrypt(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue)).getMessage();

        assertEquals("Wrong algorithm: DESede or TripleDES required", message);
    }

    @Test
    public void givenString_whenEncryptWithDES_thenReturnBase64(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "answer to everything";
        String key = "42424242";
        String initialValue = "42424242";

        String cipherValue = kryptography.encryptReturnBase64(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue);
        assertEquals("4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU", cipherValue);
    }

    @Test
    public void givenStringEncrypt_whenDecryptWithDESBase64_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "4rdeuTUMq1jGZuqSrD9Ddh8N3hBcbGwU";
        String key = "42424242";
        String initialValue = "42424242";

        String decipherValue = kryptography.decryptWithBase64(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue);
        assertEquals("answer to everything", decipherValue);
    }

    @Test
    public void givenString_whenEncryptWithDES_thenReturnHexadecimal(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToCipher = "answer to everything";
        String key = "42424242";
        String initialValue = "42424242";

        String cipherValue = kryptography.encryptReturnHexadecimal(algorithmCipher,algorithmSecretKey, valueToCipher, key, initialValue);
        assertEquals("E2B75EB9350CAB58C666EA92AC3F43761F0DDE105C6C6C14", cipherValue);
    }

    @Test
    public void  givenString_whenEncryptAES_thenReturnHexadecimal(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "answer to everything";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String cipherValue = kryptography.encryptReturnHexadecimal(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue);
        assertEquals("D24B815F37771C80ECC10A82664C92BC3F3E9F226796159182C09D8E35120457", cipherValue);
    }

    @Test
    public void givenString_whenDecryptWithDESAndHexadecimal_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.DES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.DES;
        String valueToDecipher = "E2B75EB9350CAB58C666EA92AC3F43761F0DDE105C6C6C14";
        String key = "42424242";
        String initialValue = "42424242";

        String cipherValue = kryptography.decryptWithHexadecimal(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue);
        assertEquals("answer to everything", cipherValue);
    }

    @Test
    public void  givenString_whenDecryptWithAESAndHexadecimal_thenSuccess(){
        AlgorithmCipherEnum algorithmCipher = AlgorithmCipherEnum.AES_CBC_PKCS5Padding;
        SecretKeyAlgorithmEnum algorithmSecretKey = SecretKeyAlgorithmEnum.AES;
        String valueToDecipher = "D24B815F37771C80ECC10A82664C92BC3F3E9F226796159182C09D8E35120457";
        String key = "42424242424242424242424242424242";
        String initialValue = "4242424242424242";

        String cipherValue = kryptography.decryptWithHexadecimal(algorithmCipher,algorithmSecretKey, valueToDecipher, key, initialValue);
        assertEquals("answer to everything", cipherValue);
    }
}
