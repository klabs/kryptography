package com.klabs.kryptography;

import com.klabs.kryptography.exception.DecryptException;
import com.klabs.kryptography.exception.EncryptException;
import com.klabs.kryptography.model.AlgorithmCipherEnum;
import com.klabs.kryptography.model.ReturnType;
import com.klabs.kryptography.model.SecretKeyAlgorithmEnum;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class Kryptography {

    /**
     * Encrypts a value based on a key and an initialValue.
     *
     * @param algorithmCipher the algorithm to be used to encrypt a value. See <a href="https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html</a>
     * @param valueToEncrypt the algorithm to be used to encrypt the value
     * @param key the key to be used to encrypt the value
     * @param initialValue the value used to start the iterative cipher process
     * @param algorithmSecretKey  the name of the secret-key algorithm to be associated with the given key material.
     * @deprecated This method has been discontinued because it only supports return in Base64. Now you should use {@link #encryptReturnBase64(AlgorithmCipherEnum, SecretKeyAlgorithmEnum, String, String, String)} ()}.
     */
    @Deprecated
    public String encrypt(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                                 String initialValue) throws EncryptException {
        try {
            validateInputValue(valueToEncrypt);

            Cipher cipher = Cipher.getInstance(algorithmCipher.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithmSecretKey.getAlgorithm()), new IvParameterSpec(initialValue.getBytes("UTF-8")));
            byte[] cipherText = cipher.doFinal(valueToEncrypt.getBytes());

            return Base64.getEncoder().encodeToString(cipherText);
        } catch (RuntimeException | GeneralSecurityException | UnsupportedEncodingException e) {
            throw new EncryptException(e.getMessage());
        }
    }


    /**
     * Decrypts a value based on a key and an initialValue.
     *
     * @param algorithmCipher the algorithm to be used to decrypt a value. See <a href="https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html</a>
     * @param valueToDecrypt the algorithm to be used to decrypt the value
     * @param key the key to be used to decrypt the value
     * @param initialValue the value used to start the iterative cipher process
     * @param algorithmSecretKey  the name of the secret-key algorithm to be associated with the given key material.
     * @deprecated This method has been discontinued because it only supports return in Base64. Now you should use {@link #decryptWithBase64(AlgorithmCipherEnum, SecretKeyAlgorithmEnum, String, String, String)} ()}.
     */

    public String decrypt(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToDecrypt, String key,
                                 String initialValue) throws DecryptException {
        try {
            validateInputValue(valueToDecrypt);

            Cipher cipher = Cipher.getInstance(algorithmCipher.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithmSecretKey.getAlgorithm()), new IvParameterSpec(initialValue.getBytes("UTF-8")));
            byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(valueToDecrypt));

            return new String(plainText);
        } catch (RuntimeException | GeneralSecurityException | UnsupportedEncodingException e) {
            throw new DecryptException(e.getMessage());
        }
    }

    
    public String encryptReturnBase64(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                          String initialValue) throws EncryptException {

        byte[] valueAESEncrypted = this.encryptValue(algorithmCipher, algorithmSecretKey, valueToEncrypt, key, initialValue);
        return Base64.getEncoder().encodeToString(valueAESEncrypted);
    }

    public String encryptReturnHexadecimal(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                                      String initialValue) throws EncryptException {

        byte[] valueAESEncrypted = this.encryptValue(algorithmCipher, algorithmSecretKey, valueToEncrypt, key, initialValue);
        return bytesToHex(valueAESEncrypted);
    }

    public String decryptWithBase64(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                                      String initialValue) throws EncryptException {

        return this.decryptValue(algorithmCipher, algorithmSecretKey, valueToEncrypt, key, initialValue, ReturnType.BASE64);
    }

    public String decryptWithHexadecimal(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                                    String initialValue) throws EncryptException {

        return this.decryptValue(algorithmCipher, algorithmSecretKey, valueToEncrypt, key, initialValue, ReturnType.HEXADECIMAL);
    }

    private byte[] encryptValue(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToEncrypt, String key,
                                String initialValue) throws EncryptException {
        try {
            validateInputValue(valueToEncrypt);

            Cipher cipher = Cipher.getInstance(algorithmCipher.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithmSecretKey.getAlgorithm()), new IvParameterSpec(initialValue.getBytes("UTF-8")));
            byte[] cipherText = cipher.doFinal(valueToEncrypt.getBytes());

            return cipherText;

        } catch (RuntimeException | GeneralSecurityException | UnsupportedEncodingException e) {
            throw new EncryptException(e.getMessage());
        }
    }


    private String decryptValue(AlgorithmCipherEnum algorithmCipher, SecretKeyAlgorithmEnum algorithmSecretKey, String valueToDecrypt, String key,
                          String initialValue, ReturnType returnType) throws DecryptException {
        try {
            validateInputValue(valueToDecrypt);

            Cipher cipher = Cipher.getInstance(algorithmCipher.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithmSecretKey.getAlgorithm()), new IvParameterSpec(initialValue.getBytes("UTF-8")));

            if(ReturnType.BASE64.equals(returnType))
                return new String(cipher.doFinal(Base64.getDecoder().decode(valueToDecrypt)));

            return new String(cipher.doFinal(Hex.decodeHex(valueToDecrypt.toCharArray())));

        } catch (RuntimeException | GeneralSecurityException | UnsupportedEncodingException | DecoderException e) {
            throw new DecryptException(e.getMessage());
        }
    }

    private void validateInputValue(String value){
        if(value.isEmpty() || value == null)
            throw new RuntimeException("The value provided to be decrypt/encrypt was not filled in properly.");
    }

    private String bytesToHex(byte[] bytes) {
        char[] hex = new char[bytes.length * 2];
        for (int idx = 0; idx < bytes.length; ++idx) {
            int hi = (bytes[idx] & 0xF0) >>> 4;
            int lo = (bytes[idx] & 0x0F);
            hex[idx * 2] = (char) (hi < 10 ? '0' + hi : 'A' - 10 + hi);
            hex[idx * 2 + 1] = (char) (lo < 10 ? '0' + lo : 'A' - 10 + lo);
        }
        return new String(hex);
    }
}
