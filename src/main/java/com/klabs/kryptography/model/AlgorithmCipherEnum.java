package com.klabs.kryptography.model;

public enum AlgorithmCipherEnum {
    DESede_CBC_NoPadding("DESede/CBC/NoPadding"),
    DES_CBC_PKCS5Padding("DES/CBC/PKCS5Padding"),
    AES_CBC_PKCS5Padding("AES/CBC/PKCS5Padding");

    public String algorithm;

    AlgorithmCipherEnum(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithm() {
        return algorithm;
    }
}
