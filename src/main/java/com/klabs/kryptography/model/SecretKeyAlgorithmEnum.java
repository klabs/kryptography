package com.klabs.kryptography.model;

public enum SecretKeyAlgorithmEnum {
    DESede("DESede"), DES("DES"), AES("AES");

    public String algorithm;

    SecretKeyAlgorithmEnum(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithm() {
        return algorithm;
    }

}