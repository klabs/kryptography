package com.klabs.kryptography.model;

public enum ReturnType {
    BASE64("BASE64"), HEXADECIMAL("HEXADECIMAL");

    private String returnType;

    ReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getReturnType() {
        return returnType;
    }

}
