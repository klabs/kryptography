package com.klabs.kryptography.exception;

public class DecryptException extends RuntimeException {

    public DecryptException(String message) {
        super(message);
    }

}