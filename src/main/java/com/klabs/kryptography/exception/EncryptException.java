package com.klabs.kryptography.exception;

public class EncryptException extends RuntimeException {

    public EncryptException(String message) {
        super(message);
    }

}