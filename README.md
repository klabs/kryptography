# kryptography
A **Java** library that simplifies the way to perform data encryption using Java's Cipher library.
# Usage
In your pom.xml use:

Java 8

``` xml
	<dependency>
	    <groupId>com.gitlab.klabs</groupId>
	    <artifactId>kryptography</artifactId>
	    <version>1.0.2</version>
	</dependency>
```

and

``` xml
<repositories>
   <repository>
     <id>jitpack.io</id>
     <url>https://jitpack.io</url>
   </repository>
</repositories>
```

# Generating another version

1 - Create a new branch from the master branch.

2 - Make your modifications.

3 - Update the **pom.xml** version section, the **README.md** file, tag your last commit with the new version and push everything.

4 - Merge the code into the master branch. Check the permissions to do it.

5 - Create tag from de master branch whith name version from pom.xml.

